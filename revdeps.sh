#!/bin/bash
# vim: ts=2:sw=2:
# ----------------------------------------------------------------------------
# This programme is free software and is licensed under CC0-1.0: you can 
# copy, modify, distribute and perform it, even for commercial purposes,
# without having to ask for further permission. 
# https://creativecommons.org/publicdomain/zero/1.0/legalcode
# https://spdx.org/licenses/CC0-1.0.html
# ----------------------------------------------------------------------------
#        1         2         3         4         5         6         7
#23456789012345678901234567890123456789012345678901234567890123456789012345678
#

REPOS=(solanum-test slackware64-15.0 alien-15.0)

if [ -t 1 ]; then
	_CBLUE=$'\e[32m'
	_CRESET=$'\e[0m'
else
	_CBLUE=''
	_CRESET=''
fi

fix_pattern() {
	echo -en "$1" |sed -E 's|([\+.])|\\\1|g'
}

echo "# Reverse dependencies for $1"
echo "# $(LC_ALL=C date -u)"

pattern=$(fix_pattern "$1")
for REPO in "${REPOS[@]}"; do
	find "$REPO"/so_deps -name "*.so_deps" |xargs \
		egrep "^(\w+/)?$pattern$" |sort |uniq
done |sed -E "s|([^/]+)/so_deps/(.+)\.so_deps:(.*)|\1 ${_CBLUE}\2${_CRESET} -> \3|"
