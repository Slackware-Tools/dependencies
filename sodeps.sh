#!/bin/bash
# vim: ts=2:sw=2:
# ----------------------------------------------------------------------------
# This programme is free software and is licensed under CC0-1.0: you can 
# copy, modify, distribute and perform it, even for commercial purposes,
# without having to ask for further permission. 
# https://creativecommons.org/publicdomain/zero/1.0/legalcode
# https://spdx.org/licenses/CC0-1.0.html
# ----------------------------------------------------------------------------
#        1         2         3         4         5         6         7
#23456789012345678901234567890123456789012345678901234567890123456789012345678
#

: ${REPOS:="slackware64-15.0 alien-15.0 solanum-test"}

fix_pattern() {
  echo -en "$1" |sed -E 's|([\+.])|\\\1|g'
}

pattern=$(fix_pattern $1)
for repo in $REPOS; do
	find "$repo"/shlibs -name "*.shlibs" |xargs egrep "/$pattern$" \
		|sed -E "s|$repo/shlibs/(.*)\.shlibs:.*|\1|"
done
